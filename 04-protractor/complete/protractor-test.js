'use strict';

describe('04-protractor example', function() {

  browser.get('index.html');

  it('should have initial settings', function() {
    var userNameInputElement = element(by.model('vm.user.name'));
    var userNameInput = userNameInputElement.getAttribute('value');
    expect(userNameInput).toBe('Petr');

    var userNameTextElement = element(by.binding('vm.user.name'));
    var userNameText = userNameTextElement.getText();
    expect(userNameText).toMatch('Petr');

    expect(element(by.model('vm.user.surname')).getAttribute('value')).toBe('Novák');
    expect(element(by.binding('vm.user.surname')).getText()).toBe('Příjmení: Novák');
  });

  it('should change age if model is changed', function() {
    var now = new Date();
    var expectedYear = now.getFullYear() - 1970;

    var yearOfBirthInput = element(by.model('vm.user.yearOfBirth'));
    yearOfBirthInput.clear().sendKeys(1970);

    var ageText = element(by.binding('vm.getAge()')).getText();
    expect(ageText).toMatch(expectedYear.toString());
  });

  it('should change name if model is changed', function() {
    var userNameElement = element(by.model('vm.user.name'));
    userNameElement.clear().sendKeys('Josef');

    var userNameText = element(by.binding('vm.user.name'));
    expect(userNameText.getText()).toMatch('Josef');
  });

});
