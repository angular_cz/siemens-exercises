'use strict';

function authUserNameDirective(authService) {
  return {
    scope: {},
    restrict: "A",
    link: function(scope) {
      scope.auth = authService;
    },
    template: "{{auth.user.name}}"
  };
}

function authLogoutLinkDirective(authService) {
  return {
    scope: {},
    restrict: "A",
    link: function(scope, element) {
      element.bind('click', authService.logout.bind(authService));
    }
  };
}

function authIsAuthenticatedDirective(authService) {
  return {
    scope: {},
    restrict: "A",
    link: function(scope, element) {
      scope.auth = authService;

      element.toggleClass("ng-hide", !authService.isAuthenticated());

      scope.$on("login:changedState", function() {
        element.toggleClass("ng-hide", !authService.isAuthenticated());
      });
    }
  };
}

angular.module('authApp')
  .directive("authUserName", authUserNameDirective)
  .directive("authLogoutLink", authLogoutLinkDirective)
  .directive("authIsAuthenticated", authIsAuthenticatedDirective);
