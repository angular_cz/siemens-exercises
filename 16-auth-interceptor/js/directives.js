'use strict';

function authUserNameDirective(authService) {
  return {
    scope: {},
    restrict: "A",
    link: function(scope) {
      scope.auth = authService;
    },
    template: "{{auth.user.name}}"
  };
}

function authLogoutLink(authService) {
  return {
    scope: {},
    restrict: "A",
    link: function(scope, element) {
      element.bind('click', authService.logout.bind(authService));
    }
  };
}

function authIsAuthenticated(authService) {
  return {
    scope: {},
    restrict: "A",
    link: function(scope, element) {
      scope.auth = authService;

      element.toggleClass("ng-hide", !authService.isAuthenticated());

      //TODO 8.2 změňte watch na reakci na zprávu "login:changedState"
      scope.$watch("auth.user", function() {
        element.toggleClass("ng-hide", !authService.isAuthenticated());
      });
    }
  };
}

angular.module('authApp')
  .directive("authUserName", authUserNameDirective)
  .directive("authLogoutLink", authLogoutLink)
  .directive("authIsAuthenticated", authIsAuthenticated);
