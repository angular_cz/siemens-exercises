'use strict';

function authInterceptor($injector, $rootScope, REST_URI, $q) {

  return {
    responseError: function(response) {
      return $injector.invoke(function(authModal, $http) {

        if (isRecoverable(response)) {
          // TODO 3 při úspěšném přihlášení opakujte požadavek
          // TODO 2.2 zobrazte modální okno
          return;
        }

        broadcastError(response);
        return $q.reject(response);
      });
    }
  };

  function isRecoverable(response) {
    // TODO 2.1 nastavte správný HTTP status Unauthorized
    var unauthorizedStatus = 123;
    return response.status === unauthorizedStatus && !isLoginResponse(response);
  }

  function broadcastError(response) {
    if (isLoginResponse(response)) {
      $rootScope.$broadcast('restApi:loginFailed');
    } else {
      $rootScope.$broadcast('restApi:forbidden');
    }
  }

  function isLoginResponse(response) {
    return response.config.url === REST_URI + '/login';
  }
}

function tokenInterceptor($injector) {
  return {
    request: function(config) {
      var authService = $injector.get('authService');

      if (authService.isAuthenticated()) {
        config.headers = config.headers || {};

        // TODO 9.3 do hlavičky X-Auth-Token zapište token
      }

      return config;
    }
  };
}

function configInterceptors($httpProvider) {
  $httpProvider.interceptors.push('authInterceptor');
  // TODO 9.2 přidejte tokenInterceptor
}

function configRouter($routeProvider) {

  $routeProvider

    .when('/', {
      templateUrl: 'orderList.html',
      controller: 'OrderListController',
      controllerAs: 'list',
      resolve: {
        ordersData: function(Orders) {
          return Orders.query().$promise;
        }
      }
    })

    .when('/detail/:id', {
      templateUrl: 'orderDetail.html',
      controller: 'OrderDetailController',
      controllerAs: 'detail',
      resolve: {
        orderData: function(Orders, $route) {
          var id = $route.current.params.id;

          return Orders.get({'id': id}).$promise;
        }
      }
    })

    .when('/create', {
      templateUrl: 'orderCreate.html',
      controller: 'OrderCreateController',
      controllerAs: 'create',
      resolve: {
        orderData: function(Orders) {
          return new Orders();
        },
        isAuthorized: function(authService, authModal) {

          // TODO 7.1 zobrazte modální okno pro přihlášení
          return authService.isAuthenticated();
        }
      }
    })

    .otherwise('/');
}

angular.module('authApp', ['ngRoute', 'ngMessages', 'ngResource', 'ui.bootstrap'])
  .constant('REST_URI', '//angular-cz-security-api.herokuapp.com')

  .factory("authInterceptor", authInterceptor)
  .factory("tokenInterceptor", tokenInterceptor)

  .run(function($rootScope, $location) {
    $rootScope.$on('login:loggedOut', function() {
      $location.path("/")
    });
  })

  .run(function($rootScope, $location) {
    // TODO 7.2 reagujte na zprávu $routeChangeError
    $rootScope.$on('', function() {
      $location.path('/');
    });
  })

  .run(function($rootScope, authModal) {
    $rootScope.$on('restApi:forbidden', function() {
      // TODO 6 zobrazte modální dialog - showRejectModal
    });
  })

  .config(configInterceptors)
  .config(configRouter);
