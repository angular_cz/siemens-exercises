'use strict';

function Orders(REST_URI, $resource) {
  return $resource(REST_URI + '/auth-02/orders/:id', {"id": "@id"});
}

function authService($http, REST_URI, $rootScope) {

  return {

    login: function(name, pass) {
      return $http.post(REST_URI + "/login", {name: name, password: pass})
        .then(function(response) {
          return this.checkLoginResponse_(response);
        }.bind(this));
    },

    checkLoginResponse_: function(response) {
      if (response.status == 200) {
        this.user = response.data;

        // TODO 9.1 odstraňte nastavení hlavičky
        $http.defaults.headers.common['X-Auth-Token'] = this.user.token;

        // TODO 8.1 odešlete zprávu login:changedState
      }

      return response.data;
    },

    logout: function() {
      $http.post(REST_URI + "/logout")
        .then(function() {
          delete this.user;

          // TODO 9.1 odstraňte nastavení hlavičky
          delete $http.defaults.headers.common['X-Auth-Token'];

          $rootScope.$broadcast("login:loggedOut");

          // TODO 8.1 odešlete zprávu login:changedState

        }.bind(this))
    },

    isAuthenticated: function() {
      return Boolean(this.user) || false;
    },

    getToken: function() {
      return this.user.token;
    }
  };
}

function AuthModal($modal) {

  this.modalInstance = null;

  this.createModalInstance = function(modalOptions) {
    this.modalInstance = $modal.open(modalOptions);

    this.modalInstance.result.then(function(data) {
      delete this.modalInstance;

      return data;
    }.bind(this), function() {
      delete this.modalInstance;
    }.bind(this));
  };

  this.showLoginModal = function() {
    if (!this.modalInstance) {
      this.createModalInstance({
        templateUrl: 'authModal.html',
        controller: 'AuthCtrl',
        controllerAs: 'auth'
      });
    }

    return this.modalInstance.result;
  };

  this.showRejectModal = function() {
    if (!this.modalInstance) {
      this.createModalInstance({
        templateUrl: 'rejectModal.html',
        controller: 'RejectCtrl',
        controllerAs: 'auth'
      });
    }

    return this.modalInstance.result;
  };
}

angular.module('authApp')
  .factory('Orders', Orders)
  .factory('authService', authService)
  .service('authModal', AuthModal);
