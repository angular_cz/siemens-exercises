'use strict';

describe('13-router example', function() {

  describe('redirects', function() {
    it('should redirect to list when url is invalid', function() {
      browser.get('index.html#/invalid-url')
          .then(function() {
            expect(browser.getLocationAbsUrl()).toBe('/orders');
          });
    });
  });

  describe('order list', function() {
    beforeEach(function() {
      browser.get('index.html#/orders');
    });

    it('should have six rows', function() {
      element.all(by.css('.orders tbody tr')).then(function(rows) {
        expect(rows.length).toBe(6);
      });
    });

    it('should go to detail after click on link', function() {
      element.all(by.css('.orders tbody tr td a')).first()
          .click()
          .then(function() {
            expect(element(by.css('h1')).getText())
                .toBe('Objednávka - Víťa Plšek');
          });
    });

  });

  describe('order detail', function() {
    beforeEach(function() {
      browser.get('index.html#/detail/2');
    });


    it('should go to detail after click on link', function() {
      expect(element(by.css('h1')).getText())
          .toBe('Objednávka - Milan Lempera');
    });

    it('should go to list after click back link', function() {
      element(by.css('a.back'))
          .click()
          .then(function() {
            expect(element(by.css('h3')).getText())
                .toBe('Objednávky');
          });
    });

  });

});
