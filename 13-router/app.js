'use strict';

function configRouter($routeProvider) {

  // TODO 1 definujte routu pro OrderListController
  // TODO 2 zajistěte přesměrování na seznam
  // TODO 3 definujte routu pro OrderDetailController

}

function OrderListController($http, REST_URI) {
  this.orders = [];

  this.statuses = {
    NEW: 'Nová',
    MADE: 'Vyrobená',
    CANCELLED: 'Zrušená',
    PAID: 'Zaplacená',
    SENT: 'Odeslaná'
  };

  this.onOrdersLoad = function(orders) {
    this.orders = orders;
  };

  $http.get(REST_URI + '/orders')
    .then(function(response) {
      return response.data;
    })
    .then(this.onOrdersLoad.bind(this));

}

function OrderDetailController(orderData) {
  this.order = orderData;
}

angular.module('orderAdministration', ['ngRoute'])
  .constant('REST_URI', '//angular-cz-orders-api.herokuapp.com')
  .config(configRouter)
  .controller('OrderListController', OrderListController)
  .controller('OrderDetailController', OrderDetailController);
