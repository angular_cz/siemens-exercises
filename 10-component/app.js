'use strict';

// TODO 1.2 - změňte definiční objekt na komponentu
// TODO 3.1 - přidejte závislost na komponentě tabset
var tab = {
  templateUrl: 'tab.html',
  transclude: true,

  scope: {
    header: '@'
  },
  bindToController: true,

  controllerAs: 'tab',
  controller: function() {
  }
};

// TODO 2.3 - přidejte komponentě controller
var tabset = {
  templateUrl: 'tabset.html',
  transclude: true
};

function TabsetController($log) {
  this.tabs = [];

  this.add = function(tab) {
    $log.info('Tab has been added:', tab.header);

    this.tabs.push(tab);
    this.hide(tab);

    if (this.tabs.length == 1) {
      this.select(tab);
    }
  };

  this.select = function(tab) {
    this.tabs.map(this.hide);

    tab.active = true;
  };

  this.hide = function(tab) {
    tab.active = false;
  };
}

function InfoController($log) {
  this.reportChange = function(tabHeader) {
    this.currentTabHeader = tabHeader;

    $log.info('Tab changed:', tabHeader);
  }
}

// TODO 1.1 - změňte directivu tab na komponentu
// TODO 2.1 - přidejte komponentu tabset

angular.module('directiveApp', [])

  .directive('tab', function() {return tab})
  .controller('InfoController', InfoController);
