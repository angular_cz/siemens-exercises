'use strict';

function InfoController($log) {
  this.reportChange = function(tabHeader) {
    this.currentTabHeader = tabHeader;

    $log.info('Tab changed:', tabHeader);
  }
}

var tabComponent = {
  require: {
    tabsetController: '^tabset'
  },
  templateUrl: 'tab.html',
  bindings: {
    header: '@'
  },
  transclude: true,
  controller: function() {
    this.$onInit = function() {
      this.tabsetController.add(this);
    }
  },
  controllerAs: 'tab'
};

var tabsetComponent = {
  templateUrl: 'tabset.html',
  transclude: true,
  bindings: {
    onChange: "&"
  },
  controller: TabsetController,
  controllerAs: 'tabset'
};

function TabsetController($log) {
  this.tabs = [];
  this.add = function(tab) {
    $log.info('Tab has been added:', tab.header);

    this.tabs.push(tab);
    this.hide(tab);

    if (this.tabs.length == 1) {
      this.select(tab);
    }
  };

  this.select = function(tab) {
    this.tabs.map(this.hide);

    tab.active = true;
    this.onChange({header: tab.header});
  };

  this.hide = function(tab) {
    tab.active = false;
  };
}

angular.module('componentApp', [])
  .controller('InfoController', InfoController)
  .component('tab', tabComponent)
  .component('tabset', tabsetComponent);
