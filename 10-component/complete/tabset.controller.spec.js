describe('tabset controller', function() {
  beforeEach(module('componentApp'));

  var FIRST_TAB, SECOND_TAB, THIRD_TAB;
  beforeEach(inject(function($componentController) {

    FIRST_TAB = {header: 'firstTab'};
    SECOND_TAB = {header: 'secondTab'};
    THIRD_TAB = {header: 'thirdTab'};

    var bindings = {
      onChange: jasmine.createSpy()
    };
    this.tabsetController = $componentController('tabset', {}, bindings);
  }));

  it('should contains no tabs on start', function() {
    expect(this.tabsetController.tabs.length).toBe(0);
  });

  describe('with added tabs', function() {

    beforeEach(function() {
      this.tabsetController.add(FIRST_TAB);
      this.tabsetController.add(SECOND_TAB);
      this.tabsetController.add(THIRD_TAB);
    });

    it('should have them in tabs field', function() {
      expect(this.tabsetController.tabs).toContain(FIRST_TAB);
      expect(this.tabsetController.tabs).toContain(SECOND_TAB);
      expect(this.tabsetController.tabs).toContain(THIRD_TAB);
    });

    it('should select first tab', function() {
      expect(FIRST_TAB.active).toBeTruthy();
    });

    it('should deselect added tab', function() {
      var ACTIVE_TAB = {header: 'fourth', active: true};
      this.tabsetController.add(ACTIVE_TAB);

      expect(ACTIVE_TAB.active).toBeFalsy();
    });

    it('should be able to select tab', function() {

      expect(FIRST_TAB.active).toBeTruthy();
      expect(SECOND_TAB.active).toBeFalsy();
      expect(THIRD_TAB.active).toBeFalsy();

      this.tabsetController.select(SECOND_TAB);

      expect(FIRST_TAB.active).toBeFalsy();
      expect(SECOND_TAB.active).toBeTruthy();
      expect(THIRD_TAB.active).toBeFalsy();

      this.tabsetController.select(THIRD_TAB);

      expect(FIRST_TAB.active).toBeFalsy();
      expect(SECOND_TAB.active).toBeFalsy();
      expect(THIRD_TAB.active).toBeTruthy();
    });
  });

});
