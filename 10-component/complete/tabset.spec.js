describe('tabset', function(){
  beforeEach(module('componentApp', 'componentApp.templates'));

  beforeEach(inject(function($rootScope, $compile) {

    this.$compile = $compile;
    this.$rootScope = $rootScope;

    this.$scope = this.$rootScope.$new();

    this.setTemplate = function(template) {
      this.element = angular.element('<tabset>' + template + '</tabset>');
    };

    this.render = function() {
      var template = this.$compile(this.element)(this.$scope);
      this.$scope.$digest();

      return template;
    };

    this.setTemplate(
      '<tab header="firstTab">first content</tab>' +
      '<tab header="secondTab">second content</tab>');
  }));
  
  it('contains added tabs', function() {
    var template = this.render();

    this.controller = template.controller('tabset');

    var firstTab = angular.element(template.find('tab')[0]).controller('tab');
    var secondTab = angular.element(template.find('tab')[1]).controller('tab');

    expect(this.controller.tabs).toContain(firstTab);
    expect(this.controller.tabs).toContain(secondTab);
  });

  it('should show first tab only when created', function(){
    var html = this.render().html();

    expect(html).toMatch('first content');
    expect(html).not.toMatch('second content');
  });

  it('should be able to select tab', function(){
    var template = this.render();

    var secondTab = angular.element(template.find('tab')[1]).controller('tab');

    var controller = template.controller('tabset');
    controller.select(secondTab);

    this.$scope.$digest();

    var html = template.html();
    expect(html).not.toMatch('first content');
    expect(html).toMatch('second content');
  });

  it('controller ', function(){

  });
});
