'use strict';

function InfoController($log) {
  this.reportChange = function(tabHeader) {
    this.currentTabHeader = tabHeader;

    $log.info('Tab changed:', tabHeader);
  }
}

function tabDirective() {
  return {
    require: ['^tabset', 'tab'],
    templateUrl: 'tab.html',
    transclude: true,

    scope:{},
    bindToController: {
      header: '@'
    },

    controller: function() {},
    controllerAs : 'tab',

    link: function(scope, element, attrs, controllers) {

      var tabsetController = controllers[0];
      var tabController = controllers[1];

      tabsetController.add(tabController);
    }
  }
}

function tabsetDirective() {
  return {
    templateUrl: 'tabset.html',
    transclude: true,
    scope: {},
    bindToController: {
      onChange: "&"
    },
    controller: TabsetController,
    controllerAs: 'tabset'
  };
}

function TabsetController($log) {
  this.tabs = [];
  this.add = function(tab) {
    $log.info('Tab has been added:', tab.header);

    this.tabs.push(tab);
    this.hide(tab);

    if (this.tabs.length == 1) {
      this.select(tab);
    }
  };

  this.select = function(tab) {
    this.tabs.map(this.hide);

    tab.active = true;
    this.onChange({header: tab.header});
  };

  this.hide = function(tab) {
    tab.active = false;
  };
}

angular.module('componentApp', [])
  .controller('InfoController', InfoController)
  .directive('tab', tabDirective)
  .directive('tabset', tabsetDirective);

