'use strict';

function ordersFactory(REST_URI, $resource) {
  return $resource(REST_URI + '/orders/:id', {"id": "@id"});
}

function authService($http, REST_URI, $rootScope) {
  var successLogin = function(response) {
    auth.user = response.data;

    // TODO 1.2 - přidat token do defaultní hlavičky,
    // TODO 1.3 - odeslat zprávu login:loginSuccess
  };

  var failedLogin = function(response) {
    $rootScope.$broadcast("login:loginFailed");
  };

  var auth = {
    user: null,
    login: function(name, pass) {

      var credentials = {name: name, password: pass};

      // TODO 1.1 - odeslat přihlášení
    },

    logout: function() {
      $http.post(REST_URI + "/logout").then(function() {
        auth.user = null;
        delete $http.defaults.headers.common['X-Auth-Token'];

        $rootScope.$broadcast("login:loggedOut");
      })
    },

    hasRole: function(role) {
      if (!this.user) {
        return false;
      }

      return this.user.roles.indexOf(role) !== -1;
    },

    mustHaveRole: function(role) {
      if (!this.hasRole(role)) {
        $rootScope.$broadcast("login:accessDenied")
      }
    }
  };

  return auth;
}

angular.module('authApp')
  .factory('Orders', ordersFactory)
  .factory('authService', authService);
