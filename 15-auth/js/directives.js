'use strict';

function authLogoutLinkDirective(authService) {
  return {
    scope: {},
    restrict: "A",
    link: function(scope, element) {
      element.bind('click', function() {
        // TODO 2.1 - odhlášení
      });
    }
  };
}

function authUserNameDirective(authService) {
  return {
    scope: {},
    restrict: "A",
    link: function(scope) {
      scope.auth = authService;
    },
    template: "{{auth.user.name}}"
  };
}

function authIsAuthenticatedDirective(authService) {
  return {
    scope: {},
    restrict: "A",
    link: function(scope, element) {
      scope.auth = authService;
      scope.$watch("auth.user", function(newValue, oldValue) {
        element.toggleClass("ng-hide", !scope.auth.user);
      });
    }
  };
}

function authIsAnonymousDirective(authService) {
  return {
    scope: {},
    restrict: "A",
    link: function(scope, element) {
      scope.auth = authService;
      scope.$watch("auth.user", function(newValue, oldValue) {
        element.toggleClass("ng-hide", scope.auth.user);
      });
    }
  };
}

function authHasRoleDirective(authService) {
  return {
    scope: {
      role: "@authHasRole"
    },
    restrict: "A",
    link: function(scope, element) {
      scope.auth = authService;

      scope.$watch("auth.user", function() {
        var hasRole = authService.hasRole(scope.role);
        element.toggleClass("ng-hide", !hasRole);
      });
    }
  };
}

angular.module('authApp')
  .directive("authLogoutLink", authLogoutLinkDirective)
  .directive("authUserName", authUserNameDirective)
  .directive("authIsAuthenticated", authIsAuthenticatedDirective)
  .directive("authIsAnonymous", authIsAnonymousDirective)
  .directive("authHasRole", authHasRoleDirective);
