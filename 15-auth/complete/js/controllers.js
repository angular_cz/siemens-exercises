'use strict';

function OrderListController(Orders, authService) {
  authService.mustHaveRole("ROLE_USER");

  var orderCtrl = this;
  this.auth = authService;
  this.orders = Orders.query();

  this.statuses = {
    NEW: 'Nová',
    CANCELLED: 'Zrušená',
    PAID: 'Zaplacená',
    SENT: 'Odeslaná'
  };

  this.removeOrder = function(order) {
    order.$remove(function() {
      var index = orderCtrl.orders.indexOf(order);
      orderCtrl.orders.splice(index, 1);
    });
  };

  this.updateOrder = function(order) {
    order.$save();
  };
}

function OrderDetailController(orderData, authService) {
  authService.mustHaveRole("ROLE_USER");

  this.order = orderData;
}

function OrderCreateController(orderData, $location, authService) {
  authService.mustHaveRole("ROLE_OPERATOR");

  this.order = orderData;

  this.save = function() {
    this.order.$save(function() {
      $location.path("/orders");
    });
  };
}

function HomeCtrl($scope, $location, authService) {

  this.user = authService.user;

  $scope.$on('login:loginFailed', function() {
    this.isBadLogin = true;
  }.bind(this));

  $scope.$on('login:loginSuccess', function() {
    $location.path('/orders');
    this.isBadLogin = false;
  }.bind(this));

  this.loginUser = function() {
    authService.login("user", "password");
  };

  this.loginAdmin = function() {
    authService.login("operator", "password").then(function(auth) {
    });
  };

  this.loginFail = function() {
    authService.login("badUser", "badPassword");
  };
}

angular.module('authApp')
  .controller('OrderListController', OrderListController)
  .controller('OrderDetailController', OrderDetailController)
  .controller('OrderCreateController', OrderCreateController)
  .controller('HomeCtrl', HomeCtrl);
