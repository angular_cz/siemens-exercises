'use strict';

function authUserNameDirectiveFactory(authService) {
  return {
    scope: {},
    restrict: "A",
    link: function(scope) {
      scope.auth = authService;
    },
    template: "{{auth.user.name}}"
  };
}

function authLogoutLinkFactory(authService) {
  return {
    scope: {},
    restrict: "A",
    link: function(scope, element) {
      element.bind('click', authService.logout.bind(authService));
    }
  };
}

function authIsAuthenticatedFactory(authService) {
  return {
    scope: {},
    restrict: "A",
    link: function(scope, element) {
      scope.auth = authService;
      scope.$watch("auth.user", function(newValue, oldValue) {
        element.toggleClass("ng-hide", !scope.auth.user);
      });
    }
  };
}

function authIsAnonymousFactory(authService) {
  return {
    scope: {},
    restrict: "A",
    link: function(scope, element) {
      scope.auth = authService;
      scope.$watch("auth.user", function(newValue, oldValue) {
        element.toggleClass("ng-hide", scope.auth.user);
      });
    }
  };
}

function authHasRoleFactory(authService) {
  return {
    scope: {
      role: "@authHasRole"
    },
    restrict: "A",
    link: function(scope, element) {
      scope.auth = authService;

      scope.$watch("auth.user", function() {
        var hasRole = authService.hasRole(scope.role);
        element.toggleClass("ng-hide", !hasRole);
      });
    }
  };
}

angular.module('authApp')
  .directive("authUserName", authUserNameDirectiveFactory)
  .directive("authLogoutLink", authLogoutLinkFactory)
  .directive("authIsAuthenticated", authIsAuthenticatedFactory)
  .directive("authIsAnonymous", authIsAnonymousFactory)
  .directive("authHasRole", authHasRoleFactory);
